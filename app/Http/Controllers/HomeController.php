<?php

namespace App\Http\Controllers;

use App\Repositories\Menus\ModuleProfileRepository;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    protected  $moduleProfileRepository;


    public function __invoke(){}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ModuleProfileRepository $repo)
    {
        $this->moduleProfileRepository = $repo;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $resp =
            [
                'menu' => $this->moduleProfileRepository->findModules(session('user')["id_profile"]),
            ];

        session(['resp' => $resp]);

        return View::make('templates.default')
            ->with('resp', $resp)
            ->with('page','layouts.app');
    }

}
