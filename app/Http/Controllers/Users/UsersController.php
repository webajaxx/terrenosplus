<?php

namespace App\Http\Controllers\Users;


use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;

class UsersController extends AppBaseController
{

    /** @var $userRepository UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    public function index(Request $request)
    {

        $users = $this->userRepository->all();

        return View::make('templates.default')
            ->with('resp', session('resp'))
            ->with('users' ,$users)
            ->with('page' ,'users.index');

    }

}
