<?php

namespace App\Models\Menus;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property-read int $id
 * @property string $profile
 * @property int $viewall
 * @property string $icon
 * @property string $nameprofile
 * @property-read User $user
 */

class Profile extends Model
{
    use SoftDeletes;

    protected $table = 'profile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile',
        'viewall',
        'icon',
        'nameprofile'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'deleted_at' => 'datetime',
    ];

    /**
     * @return HasOne
     */
    public  function  user() : HasOne
    {
        return $this->hasOne(User::class, 'idprofile');
    }

    public function  moduleProfile():HasMany
    {
        return $this->HasMany(ModuleProfile::class,'id_profile');
    }
}
