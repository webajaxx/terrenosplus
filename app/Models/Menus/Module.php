<?php

namespace App\Models\Menus;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Collection\Collection;

/**
 * @property-read int $id
 * @property string $name
 * @property string $link
 * @property string $order_module
 * @property string $icon_module
 * @property string $color
 * @property string $color_text
 * @property-read  Collection|SubMenu $submenu
 *
 */

class Module extends Model
{
    use SoftDeletes;

    protected $table = 'module';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'link',
        'order_module',
        'icon_module',
        'color',
        'color_text',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_module' => 'integer'
    ];


    /**
     * @return HasMany
     */
    public function  moduleProfile():HasMany
    {
        return $this->hasMany(ModuleProfile::class,'id_module');
    }

    /**
     * @return BelongsTo
     */
    public function submenu() :HasMany
    {
        return $this->hasMany(SubMenu::class,'id_module');
    }
}
