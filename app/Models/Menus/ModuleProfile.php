<?php

namespace App\Models\Menus;


use App\Models\Menus\Module;
use App\Models\Menus\Profile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneOrMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @property-read int $id
 * @property int $id_profile
 * @property int $id_module
 * @property-read  Collection|Profile[] $profile
 * @property Collection|Module[] $modules
 */

class ModuleProfile extends Model
{
    use SoftDeletes;

    protected $table = 'module_profile';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_profile' => 'integer',
        'id_module' => 'integer',
    ];


    /**
     * @return HasMany
     */
    public  function  module() : HasMany
    {
        return $this->HasMany(Module::class,'id');
    }

    /**
     * @return HasOne
     */
    public  function profile() :HasOne
    {
        return $this->hasOne(Profile::class , 'id');
    }
}
