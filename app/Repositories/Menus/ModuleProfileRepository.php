<?php

namespace App\Repositories\Menus;


use App\Models\Menus\Module;
use App\Models\Menus\ModuleProfile;
use App\Repositories\BaseRepository;
use Flash;
use Illuminate\Support\Collection;

/**
 * Class ModuleProfileRepository
 * @package App\Repositories\Menus\ModuleProfileRepository
 * @version August 30, 2021, 10:24 pm UTC
 */
class ModuleProfileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'id_profile',
        'id_module'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ModuleProfile::class;
    }

    public function find($id, $columns = ['*'])
    {
        $query = $this
            ->model
            ->newQuery()
            ->where('id_profile',$id);

        return $query->find($id, $columns);

    }


    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function findModules($id): object
    {
        return
           $this
            ->model
            ->newQuery()
            ->where('id_profile',$id)
            ->with(['module'])
            ->with(['module.submenu'])
            ->with(['profile'])
            ->get();
    }

}

