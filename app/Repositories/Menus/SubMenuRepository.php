<?php

namespace App\Repositories\Menus;

use App\Models\Menus\Module;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Relations\HasOne;

class SubMenuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'profile',
        'icon',
        'nameprofile',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Profile::class;
    }



    public function find($id, $columns = ['*'])
    {
        $query = $this
            ->model
            ->newQuery()
            ->where('id',$id);


        return $query->find($id, $columns);

    }

}
