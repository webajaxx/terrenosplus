<?php

namespace App\Repositories\Menus;


use App\Models\Menus\Profile;
use App\Repositories\BaseRepository;
use Flash;
use Illuminate\Support\Collection;

/**
 * Class ProfileRepository
 * @package App\Repositories\Menus\ProfileRepository
 * @version August 30, 2021, 10:24 pm UTC
 */
class ProfileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'profile',
        'icon',
        'nameprofile',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Profile::class;
    }



    public function find($id, $columns = ['*'])
    {
        $query = $this
            ->model
            ->newQuery()
            ->where('id',$id);


        return $query->find($id, $columns);

    }



}

