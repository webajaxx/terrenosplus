<?php

namespace App\Repositories\Menus;

use App\Models\Menus\Module;
use App\Repositories\BaseRepository;

class ModuleRepository extends BaseRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'link',
        'icon',
        'order_module',
        'icon_module',
        'color',
        'color_text',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Module::class;
    }



    public function find($id, $columns = ['*'])
    {
        $query = $this
            ->model
            ->newQuery();



        return $query->find($id, $columns);

    }



}
