<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('module')) {
            return;
        }

        Schema::create('module', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('link')->nullable(false) ;
            $table->string('icon')->nullable(false) ;
            $table->integer('order_module')->nullable(false) ;
            $table->string('icon_module')->nullable(false) ;
            $table->string('color')->nullable(false) ;
            $table->string('color_text')->nullable(false) ;
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('module', function (Blueprint $table) {

        });
    }
}
