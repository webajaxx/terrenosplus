<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('submenu')) {
            return;
        }

        Schema::create('submenu', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('link')->nullable(false) ;
            $table->string('page')->nullable(false) ;
            $table->string('icon')->nullable(false) ;
            $table->integer('order')->nullable(false) ;
            $table->integer('id_module')->unsigned();
            $table->foreign('id_module')->references('id')->on('module');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('submenu', function (Blueprint $table) {
            //
        });
    }
}
