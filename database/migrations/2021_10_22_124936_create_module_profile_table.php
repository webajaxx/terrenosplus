<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('moduleProfile')) {
            return;
        }

        Schema::create('moduleProfile', function (Blueprint $table) {
            $table->id();
            $table->integer('id_profile')->unsigned() ;
            $table->integer('id_module')->unsigned();
            $table->foreign('id_profile')->references('id')->on('profile');
            $table->foreign('id_module')->references('id')->on('module');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moduleProfile', function (Blueprint $table) {
            //
        });
    }
}
