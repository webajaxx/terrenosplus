
    <div id="wrapper">
        <div id="page-wrapper" class="blue-bg">
        <div class="row border-bottom blue-bg">
        <nav class="navbar navbar-expand-lg navbar-static-top" role="navigation">

                <a href="#" class="navbar-brand blue-bg">Logo </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-reorder"></i>
                </button>

            <div class="navbar-collapse collapse " id="navbar">
                <ul class="nav navbar-nav mr-auto col-md-10">
                  @foreach ($resp["menu"] as $item)
                        @foreach ($item->module as $itens)
                     <li class="dropdown">
                         <p class="d-none"   >{{$dropdown = "off"}}</p>
                                @if($itens->id === 1)
                                   <a  role="button" href="{{ URL::to($itens->link) }}" >{{$itens->name}}</a>
                                @else

                                     <!-- se tiver submenu coloca o icone do dropdown no menu principal -->
                                     @foreach ($item['module'] as $value)
                                         @foreach ($value['submenu'] as $values)
                                                <a  role="button" href="{{ URL::to($itens->link) }}" class="dropdown-toggle" data-toggle="dropdown">{{$itens->name}}</a>
                                                <a class="d-none" > {{$dropdown = "on"}} </a>
                                         @endforeach
                                     @endforeach

                                    <!-- se não tiver submenu deixa sem o dropdown -->
                                    @if($dropdown === "off")
                                        <a  role="button" href="{{ URL::to($itens->link) }}" >{{$itens->name}}</a>
                                    @endif

                                @endif
                        @endforeach
                          <!-- carrega o submenu -->
                          @foreach ($item['module'] as $value)
                             @foreach ($value['submenu'] as $values)
                                @if($values->id_module === $itens->id)
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="{{ URL::to($values->link) }}">{{$values->name}}</a></li>
                                    </ul>
                                @endif
                             @endforeach
                          @endforeach
                     </li>
                  @endforeach
                </ul>
                <ul class="navbar-collapse collapse " style="margin-top:1.5%">
                    <!-- Users image -->
                    <li class="dropdown">
                        <div class="dropdown profile-element">
                           <!-- <img alt="image" class="rounded-circle" src="img/profile_small.jpg">-->
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                <span class="text-muted text-xs block"> <i class="fa fa-user">

                                        </i> Perfil Principal: {{ $resp["menu"][0]["profile"]["profile"] }}
                                            <b class="caret"></b>
                                </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs" x-placement="bottom-start" style="position: absolute;  left: 0px; will-change: top, left;">
                                <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                </li>
                              </ul>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                     </li>
                </ul>
            </div>
        </nav>
        </div>
         @include($page) <!--page dinamica-->
  </div>
</div>


