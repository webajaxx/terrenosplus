<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Terrenos Plus</title>

    <link href="{{ URL::to('assets/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/style.css') }}" rel="stylesheet">

    <!--ajust elements datatable-->
    <link rel="stylesheet" href="{{ URL::to('assets/datatables/css/datatables.topbottom.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Toastr -->
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/toastr/toastr.min.css') }}">


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


    <!--jAlert css-->
    <link rel="stylesheet" href="{{ URL::to('jalert/css/jAlert.css') }}">

</head>

<body class="top-navigation">


        <!--js-->
        <script src="{{ URL::to('assets/plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/sparklines/sparkline.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/jquery-knob/jquery.knob.min.js ') }}"></script>
        <script src="{{ URL::to('assets/plugins/moment/moment.min.js' )}}"></script>
        <script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

        <!-- SweetAlert2 -->
        <script src="{{ URL::to('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/plugins/toastr/toastr.min.js') }}"></script>
            <!-- Toastr -->

        <script src="{{ URL::to('assets/dist/js/adminlte.js') }}"></script>
        <script src="{{ URL::to('assets/dist/js/pages/dashboard.js') }}"></script>
        <script src="{{ URL::to('assets/dist/js/demo.js') }}"></script>

      <script src="{{ URL::to('assets/js/popper.min.js') }}"></script>
      <script src="{{ URL::to('assets/js/bootstrap.js') }}"></script>
      <script src="{{ URL::to('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
      <script src="{{ URL::to('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

      <!-- Custom and plugin javascript -->
      <script src="{{ URL::to('assets/js/inspinia.js') }}"></script>
      <script src="{{ URL::to('assets/js/plugins/pace/assets/pace.min.js') }}"></script>

      <!-- Flot -->
      <script src="{{ URL::to('assets/js/plugins/flot/jquery.flot.js') }}"></script>
      <script src="{{ URL::to('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
      <script src="{{ URL::to('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>

      <!-- ChartJS-->
      <script src="{{ URL::to('assets/js/plugins/chartJs/Chart.min.js') }}"></script>

      <!-- Peity -->
      <script src="{{ URL::to('assets/js/plugins/peity/jquery.peity.min.js') }}"></script>
      <!-- Peity demo -->
      <script src="{{ URL::to('assets/js/demo/peity-demo.js') }}"></script>


    <!-- SweetAlert2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="{{ URL::to('assets/plugins/toastr/toastr.min.js') }}"></script>

      <!--jAlert-->
      <script type="text/javascript" src="{{ URL::to('assets/jalert/js/jAlert.js') }}"></script>
      <script type="text/javascript" src="{{ URL::to('assets/jalert/js/jAlert-functions.js') }}"></script>




    <!--DATATABLE Buttons-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js">
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>




  @include('templates.header')
  @include('templates.footer')

</body>

</html>
