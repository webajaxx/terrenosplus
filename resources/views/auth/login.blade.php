<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Terrenos Plus - Login</title>

    <link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/iCheck/custom.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/style.css') }}">

    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
</head>

<style>
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: #1c84c6;
        color: white;
        text-align: left;
    }
</style>

<body class="blue-bg">
    <div class="loginColumns animated fadeInDown" style="margin-left: 37%;margin-top: 5%">

        <div class="row">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>


            <div class="ibox-content">
                <h1><strong>Backoffice</strong> - Digite a Senha</h1>
                <br>
                <form method="post" action="{{ url('/login') }}">
                    @csrf
                    <div class="m-t {{ $errors->has('email') ? ' has-error' : '' }}" >
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                               placeholder="E-mail">
                        <span class="glyphicon glyphicon-envelope form-control-feedback" ></span>
                        @if ($errors->has('email'))
                            <span class="help-block" >
                                                        <strong style="color:darkred">{{ $errors->first('email') }}</strong>
                                                    </span>
                        @endif
                    </div>
                    <br>
                    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Senha" name="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback" style="color:darkred"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                        @endif

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox icheck" >
                                <label style="color: black">
                                    <input  type="checkbox"  name="remember"> Manter-se conectado
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4 " style="margin-left: 28%">
                            <button type="submit" class="btn btn-primary btn-block btn-flat ">Entrar</button>
                        </div>
                        <!-- /.col -->
                    </div>

                </form>

            </div>

        </div>
    </div>


    <div class="row footer">
        <div class="col-md-6">
            Copyright Urbit® 2021
        </div>
        <div class="col-md-6 text-right">
            <small>Versão 1.0.1</small>
        </div>
    </div>
</body>

</html>

<!-- Mainly scripts -->
<script src="{{ URL::to('assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ URL::to('assets/js/popper.min.js ')}}"></script>
<script src="{{ URL::to('assets/js/bootstrap.js') }}"></script>
<script src="{{ URL::to('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ URL::to('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ URL::to('assets/js/inspinia.js ')}}"></script>
<script src="{{ URL::to('assets/js/plugins/pace/pace.min.js') }}"></script>

<!-- iCheck -->
<script src="{{ URL::to('assets/js/plugins/iCheck/icheck.min.js') }}"></script>
